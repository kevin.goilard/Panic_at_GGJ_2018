﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : ColoredObject
{
    [SerializeField]
    private Vector2 teleportTo;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Player>())
        {
            collision.transform.position = teleportTo;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(teleportTo, 0.2f);
        Gizmos.DrawLine(teleportTo, transform.position);
    }
}