﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private GameObject player;

    private Vector3 offset;
    private int width;
    private int height;

    [SerializeField]
    private Vector3 minPosition = new Vector3(-20f, -1.47f, 0f); //position en bas à gauche de fin de niveau

    [SerializeField]
    private Vector3 maxPosition = new Vector3(20f, 7f, 0f); //position en haut à droite de fin de niveau

    [SerializeField]
    private float zPosition = -10;

    [SerializeField]
    private float yPosition = 0;

    // Use this for initialization
    private void Start()
    {
        if (Player.Instance != null)
        {
            player = Player.Instance.gameObject;
            offset = new Vector3(0, 0, zPosition);

            width = Screen.width / 100;
            height = Screen.height / 100;
            minPosition += new Vector3(width / 2, height / 2, 0f);
            maxPosition -= new Vector3(width / 2, height / 2, 0f);
            if (player.transform.position.x < minPosition.x)
            {
                if (player.transform.position.y < minPosition.y)
                    transform.position = new Vector3(minPosition.x, minPosition.y, zPosition);
                else if (player.transform.position.y > maxPosition.y)
                    transform.position = new Vector3(minPosition.x, maxPosition.y, zPosition);
                else
                    transform.position = new Vector3(minPosition.x, transform.position.y, zPosition);
            }
            else if (player.transform.position.x > maxPosition.x)
            {
                if (player.transform.position.y < minPosition.y)
                    transform.position = new Vector3(maxPosition.x, minPosition.y, zPosition);
                else if (player.transform.position.y > maxPosition.y)
                    transform.position = new Vector3(maxPosition.x, maxPosition.y, zPosition);
                else
                    transform.position = new Vector3(maxPosition.x, transform.position.y, zPosition);
            }
            else
                transform.position = player.transform.position + offset;
        }
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        if (player != null)
        {
            Vector3 newPosition = player.transform.position + offset;

            bool moveLeft = newPosition.x < transform.position.x - width / 12;
            bool moveDown = newPosition.y < transform.position.y - height / 12;
            bool moveRight = newPosition.x > transform.position.x + width / 12;
            bool moveUp = newPosition.y > transform.position.y + height / 12;
            /*
            Debug.Log("minPosition = " + minPosition);
            Debug.Log("newPosition = " + newPosition);
            Debug.Log("positionActuel = " + transform.position);
            */
            if (newPosition.x < maxPosition.x && newPosition.x > minPosition.x && (moveLeft || moveRight))
            {
                transform.position = new Vector3(newPosition.x, transform.position.y, zPosition);
                if (moveLeft)
                    transform.position += new Vector3(width / 12, 0, 0);
                else
                    transform.position -= new Vector3(width / 12, 0, 0);
            }
            if (newPosition.y < maxPosition.y && newPosition.y > minPosition.y && (moveDown || moveUp))
            {
                transform.position = new Vector3(transform.position.x, newPosition.y, zPosition);

                if (moveDown)
                    transform.position += new Vector3(0, height / 12, 0);
                else
                    transform.position -= new Vector3(0, height / 12, 0);
            }
        }
        else
            Start();
    }
}