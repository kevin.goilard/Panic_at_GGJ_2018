﻿using System.Collections;
using System.Collections.Generic;
using uCPf;
using UnityEngine;

public class DebugMenu : MonoBehaviour
{
    /*
	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {
	}*/

    private Player player;

    [SerializeField]
    private ColorPicker allyPicker;

    [SerializeField]
    private ColorPicker ennemyPicker;

    [SerializeField]
    private ColorPicker neutralPicker;

    [SerializeField]
    private string allyColor;

    [SerializeField]
    private string ennemyColor;

    [SerializeField]
    private string neutralColor;

    public void Update()
    {
        if (player == null)
        {
            player = Player.Instance;
        }
    }

    public void AllyColorChanged(Color newColor)
    {
        EventManager.Raise<Color>(Enums.EventTypeEnum.ALLY_COLOR_CHANGED, newColor);
    }

    public void EnnemyColorChanged(Color newColor)
    {
        EventManager.Raise<Color>(Enums.EventTypeEnum.ENNEMI_COLOR_CHANGED, newColor);
    }

    public void NeutralColorChanged(Color newColor)
    {
        EventManager.Raise<Color>(Enums.EventTypeEnum.NEUTRAL_COLOR_CHANGED, newColor);
    }

    public void TimeScaleChanged(float newValue)
    {
        Time.timeScale = newValue;
    }

    public void CharacterScaleChanged(float newScale)
    {
        player.transform.localScale = new Vector3(newScale, newScale, 1);
    }

    public void ResetParams()
    {
        allyPicker.SetColorByColorCode(allyColor);
        ennemyPicker.SetColorByColorCode(ennemyColor);
        neutralPicker.SetColorByColorCode(neutralColor);
        Time.timeScale = 1;
        player.transform.localScale = Vector3.one;
    }

    public static void Quit()
    {
        //TODO : Return to MainMenu
    }
}