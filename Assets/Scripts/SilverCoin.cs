﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SilverCoin : Coin
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Player>() != null)
        {
            SoundManager.PlaySoundEffectIns(Enums.SoundEffect.PIECE);
            Player.coins++;
            Destroy(gameObject);
        }
    }
}