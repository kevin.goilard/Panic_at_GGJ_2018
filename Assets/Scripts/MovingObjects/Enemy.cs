﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Classe de base des ennemis
/// </summary>
public class Enemy : MovingObject
{
    private bool direction = false;

    protected Coroutine jumpCooldown;
    protected Coroutine autoActionCorout;
    protected Coroutine autoAttackCorout;

    protected override void OverridableStart()
    {
        base.OverridableStart();
        for (int i = 0; i < Enum.GetNames(typeof(Enums.Skills)).Length; i++)
        {
            capacities.Add(false);
        }
        rbody.velocity = new Vector2(maxSpeed, 0);
        animator.SetBool("IsWalking", true);
    }

    // Update is called once per frame
    private void Update()
    {
        if (autoActionCorout == null && (capacities[(int)Enums.Skills.DOUBLE_JUMP] || capacities[(int)Enums.Skills.DASH]))
            autoActionCorout = StartCoroutine(AutoActionCoroutine());
        if (autoAttackCorout == null && capacities[(int)Enums.Skills.SWORD_HIT])
            autoAttackCorout = StartCoroutine(AutoAttackoroutine());
    }

    private void LateUpdate()
    {
        rbody.velocity = new Vector2((direction ? 1 : -1) * maxSpeed, rbody.velocity.y);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<SkillProjectile>() == null)
        {
            if (capacities[(int)Enums.Skills.DASH])
            {
                Dasher();
            }
            else if (capacities[(int)Enums.Skills.DOUBLE_JUMP] && !InAir && jumpCooldown == null)
            {
                Jump();
                jumpCooldown = StartCoroutine(EnemyJumpCooldown());
            }
            else
            {
                direction = !direction;
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, 1);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Player tmp = collision.gameObject.GetComponent<Player>();
        if (tmp != null)
            tmp.Die();
        if (collision.gameObject.GetComponent<StaticPlatform>() == null && collision.gameObject.GetComponent<MovingPlatform>() == null
            && collision.gameObject.GetComponent<EnemyUniqueMovingPlatform>() == null && collision.gameObject.GetComponent<UniqueMovingPlatform>() == null
            && collision.gameObject.GetComponent<Ground>() == null && collision.gameObject.GetComponent<SkillProjectile>() == null)
        {
            direction = !direction;
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, 1);
        }
    }

    public override void Die()
    {
        for (int i = 0; i < capacities.Count; i++)
        {
            if (capacities[i])
            {
                LaunchCapa((Enums.Skills)(i));
            }
        }
        SoundManager.PlaySoundEffectIns(Enums.SoundEffect.ENEMY_HIT);

        StopAllCoroutines();
        Destroy(gameObject);
    }

    protected override void LaunchCapa(Enums.Skills skill)
    {
        if (capacities[(int)skill])
        {
            GameObject tmp = Instantiate(projectilePrefab);
            tmp.transform.position = new Vector3(transform.position.x - GetComponent<SpriteRenderer>().size.x / 2 - (transform.localScale.x / Mathf.Abs(transform.localScale.x)), transform.position.y, 0);
            tmp.GetComponent<SkillProjectile>().InitRandom(skill, -(int)(transform.localScale.x / Mathf.Abs(transform.localScale.x)));
            capacities[(int)skill] = false;
        }
    }

    private IEnumerator AutoActionCoroutine()
    {
        float lastTime = 0;
        bool lastWasJump = false;
        while (capacities[(int)Enums.Skills.DOUBLE_JUMP] || capacities[(int)Enums.Skills.DASH])
        {
            if (Time.time - 2 > lastTime)
            {
                if (lastWasJump && capacities[(int)Enums.Skills.DASH])
                {
                    Dasher();
                    lastWasJump = false;
                }
                else if (capacities[(int)Enums.Skills.DOUBLE_JUMP])
                {
                    Jump();
                    lastWasJump = true;
                }
                else if (capacities[(int)Enums.Skills.DASH])
                {
                    Dasher();
                    lastWasJump = false;
                }
                lastTime = Time.time;
            }
            yield return null;
        }
        autoActionCorout = null;
    }

    private IEnumerator AutoAttackoroutine()
    {
        float lastTime = 0;
        while (capacities[(int)Enums.Skills.SWORD_HIT])
        {
            if (Time.time - 3 > lastTime)
            {
                animator.SetTrigger("Attack");
                SoundManager.PlaySoundEffectIns(Enums.SoundEffect.ATTACK);
                lastTime = Time.time;
            }
            yield return null;
        }
        autoAttackCorout = null;
    }

    private IEnumerator EnemyJumpCooldown()
    {
        float startingTime = 0;
        while (startingTime + 2 < Time.time)
        {
            yield return null;
        }
        jumpCooldown = null;
    }
}