﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UniqueMovingPlatform : MovingObject
{
    private Vector3 target;

    [SerializeField]
    private List<Vector3> targetlist = new List<Vector3>();

    private int index = 0;
    private bool hasFinished = false;
    private bool canMove = false;

    protected override void OverridableStart()
    {
        base.OverridableStart();
    }

    private void LateUpdate()
    {
        if (Vector2.Distance(transform.position, target) < 0.2f && canMove)
            ChangeTarget();
        if (target != null && !hasFinished && canMove)
            rbody.velocity = (target - transform.position).normalized * maxSpeed;
        else if (rbody.velocity != Vector2.zero)
        {
            rbody.velocity = Vector2.zero;
            rbody.constraints = RigidbodyConstraints2D.FreezeAll;
        }
    }

    public override void Die()
    {
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<Player>() != null && !canMove)
        {
            target = targetlist[0];
            canMove = true;
            rbody.constraints = RigidbodyConstraints2D.FreezeRotation;
        }
    }

    private void ChangeTarget()
    {
        if (!hasFinished && index + 1 < targetlist.Count)
        {
            index++;
            target = targetlist[index];
        }
        else
        {
            hasFinished = true;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        for (int i = 0; i < targetlist.Count; i++)
        {
            Gizmos.DrawSphere(targetlist[i], 0.2f);
        }
        for (int i = 1; i < targetlist.Count; i++)
        {
            Gizmos.DrawLine(targetlist[i - 1], targetlist[i]);
        }
    }
}