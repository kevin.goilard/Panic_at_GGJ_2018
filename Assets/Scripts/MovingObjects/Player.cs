﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Le joueur \o/ Praise the Sun \[T]/
/// </summary>
public class Player : MovingObject
{
    [SerializeField]
    protected KeyCode left = KeyCode.Q;

    [SerializeField]
    protected KeyCode right = KeyCode.D;

    [SerializeField]
    protected KeyCode jump = KeyCode.Space;

    [SerializeField]
    protected KeyCode attack = KeyCode.E;

    [SerializeField]
    protected KeyCode dash = KeyCode.A;

    [SerializeField]
    protected KeyCode doubleJump = KeyCode.Z;

    [SerializeField]
    protected bool canUse = true;

    [SerializeField]
    protected Transition transition;

    public static Player Instance;

    public static int coins = 0;

    public static int enemyDash = 0;
    public static int enemyJump = 0;
    public static int enemyKill = 0;

    protected override void OverridableStart()
    {
        Instance = this;
        base.OverridableStart();
        for (int i = 0; i < Enum.GetNames(typeof(Enums.Skills)).Length; i++)
        {
            capacities.Add(true);
        }
    }

    // Update is called once per frame
    private void Update()
    {
        if (dashCorout == null)
        {
            OverridableUpdate();
            if (Input.GetKey(left) || Input.GetKey(right))
                animator.SetBool("IsWalking", true);
            if (!Input.GetKey(left) && !Input.GetKey(right) && animator.GetBool("IsWalking"))
                animator.SetBool("IsWalking", false);
            if (Input.GetKeyDown(left))
                transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, 1);
            if (Input.GetKeyDown(right))
                transform.localScale = new Vector3(-Mathf.Abs(transform.localScale.x), transform.localScale.y, 1);
            if (Input.GetKeyUp(right) && Input.GetKey(left))
                transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, 1);
            if (Input.GetKeyDown(left) && Input.GetKey(right))
                transform.localScale = new Vector3(-Mathf.Abs(transform.localScale.x), transform.localScale.y, 1);

            movement.x = (Input.GetKey(left) ? -speed : 0) + (Input.GetKey(right) ? speed : 0);
            Move(movement);
            if ((Input.GetKeyDown(jump) || Input.GetKeyDown(doubleJump)) && !nonInteractible)
            {
                if (!InAir && (canUse || Input.GetKeyDown(jump)))
                {
                    Jump();
                }
                if (capacities[(int)(Enums.Skills.DASH)] && !HasDoubleJump && InAir && (Input.GetKeyDown(doubleJump) || Input.GetKeyDown(jump)) && canUse)
                {
                    Jump();
                    HasDoubleJump = true;
                }
                else if ((Input.GetKeyDown(doubleJump)) && !canUse)
                {
                    LaunchCapa(Enums.Skills.DOUBLE_JUMP);
                }
            }
            if (Input.GetKeyDown(attack) && !nonInteractible)
            {
                if (canUse)
                {
                    SoundManager.PlaySoundEffectIns(Enums.SoundEffect.ATTACK);
                    animator.SetTrigger("Attack");
                }
                if (!canUse)
                {
                    LaunchCapa(Enums.Skills.SWORD_HIT);
                }
            }
            if (Input.GetKeyDown(dash) && capacities[(int)(Enums.Skills.DASH)] && !nonInteractible)
            {
                if (canUse)
                {
                    SoundManager.PlaySoundEffectIns(Enums.SoundEffect.DASH_PLAYER);
                    Dasher();
                }
                else
                {
                    LaunchCapa(Enums.Skills.DASH);
                }
            }
        }
    }

    protected override void LaunchCapa(Enums.Skills skill)
    {
        if (capacities[(int)skill])
        {
            GameObject tmp = Instantiate(projectilePrefab);
            tmp.transform.position = new Vector3(transform.position.x - GetComponent<SpriteRenderer>().size.x - (transform.localScale.x / Mathf.Abs(transform.localScale.x)), transform.position.y, 0);
            tmp.GetComponent<SkillProjectile>().Init(skill, -(int)(transform.localScale.x / Mathf.Abs(transform.localScale.x)));
            capacities[(int)skill] = false;
        }
    }

    public override void Die()
    {
        if (nonInteractible)
            return;
        rbody.constraints = RigidbodyConstraints2D.FreezeAll;
        animator.SetTrigger("Death");
        StartCoroutine(DeathDelay());
    }

    public IEnumerator DeathDelay()
    {
        nonInteractible = true;
        SoundManager.PlaySoundEffectIns(Enums.SoundEffect.PLAYER_HIT);
        SoundManager.PlaySoundEffectIns(Enums.SoundEffect.GAMEOVER);
        float startTime = Time.time;
        while (Time.time < startTime + 1)
        {
            yield return null;
        }

        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
    }

    public void MakeTransition(int id)
    {
        Debug.Log("fdv");
        if (nonInteractible)
            return;
        nonInteractible = true;
        rbody.constraints = RigidbodyConstraints2D.FreezePositionX;
        transition.ChangeScene(id);
    }

    public void MakeTransitionBis(int id)
    {
        if (nonInteractible)
            return;
        nonInteractible = true;
        rbody.constraints = RigidbodyConstraints2D.FreezePositionX;
        transition.ChangeSceneBis(id);
    }
}