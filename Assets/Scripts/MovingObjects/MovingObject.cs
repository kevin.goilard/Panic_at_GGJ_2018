﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Classe mère des objects qui peuvent avoir des compétences (joueur, ennemis...)
/// </summary>
public class MovingObject : ColoredObject
{
    public Rigidbody2D rbody;

    protected Vector2 movement = Vector2.zero;

    [SerializeField]
    protected GameObject projectilePrefab;

    [SerializeField]
    protected float speed = 15;

    [SerializeField]
    protected float jumpPower = 150;

    [SerializeField]
    protected float dashPower = 35;

    [SerializeField]
    protected float maxSpeed = 2;

    [SerializeField]
    protected float dashDuration = 2;

    protected Animator animator;
    protected List<bool> capacities = new List<bool>();
    protected Coroutine dashCorout;
    protected bool nonInteractible = false;

    protected bool hasJumped = false;
    protected bool hasDashed = false;

    private bool inAir = false;
    private bool hasDoubleJump = false;
    private bool isOnPlatform = false;
    private int atterit = 0;

    public bool InAir
    {
        get
        {
            return inAir;
        }

        set
        {
            inAir = value;
        }
    }

    public bool HasDoubleJump
    {
        get
        {
            return hasDoubleJump;
        }

        set
        {
            hasDoubleJump = value;
        }
    }

    protected override void OverridableStart()
    {
        rbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        if (rbody == null)
            Debug.LogError("Attach a rigidbody to enable movement");
    }

    // Update is called once per frame
    private void Update()
    {
        OverridableUpdate();
    }

    private void FixedUpdate()
    {
        if (Mathf.Abs(rbody.velocity.x) > maxSpeed)
        {
            rbody.velocity = new Vector2((rbody.velocity.x / Mathf.Abs(rbody.velocity.x)) * maxSpeed, rbody.velocity.y);
        }
    }

    protected virtual void OverridableUpdate()
    {
        bool debutAir = InAir;

        if (isOnPlatform)
            InAir = false;
        if (rbody != null && !isOnPlatform)
            InAir = Mathf.Abs(rbody.velocity.y) > 0.00001f;

        if (atterit > 0 && !InAir)
        {
            atterit++;
            if (atterit > 2 && !nonInteractible)
            {
                atterit = 0;
                SoundManager.PlaySoundEffectIns(Enums.SoundEffect.RECEPTION_SAUT);
                HasDoubleJump = false;
            }
        }
        else
            atterit = 0;

        if (!debutAir)
            return;
        if (debutAir != InAir)
            atterit++;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<MovingPlatform>() != null || collision.gameObject.GetComponent<UniqueMovingPlatform>() != null)
        {
            isOnPlatform = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<MovingPlatform>() != null || collision.gameObject.GetComponent<UniqueMovingPlatform>() != null)
        {
            isOnPlatform = false;
        }
    }

    /// <summary>
    /// Ajoute une force à l'objet pour lui dire de se déplacer dans une certaine direction
    /// </summary>
    /// <param name="forceToAdd">Force à ajouter au momemtum</param>
    public void Move(Vector2 forceToAdd)
    {
        if (rbody != null)
        {
            rbody.AddForce(forceToAdd);
        }
    }

    /// <summary>
    /// Fait sauter. Duh
    /// </summary>
    public void Jump()
    {
        hasJumped = true;
        hasDashed = false;
        animator.SetTrigger("Jump");
        Move(new Vector2(0, jumpPower));
    }

    /// <summary>
    /// Surprise, ça fait attaquer \o/
    /// </summary>
    public void Attack()
    {
        //nothing here
    }

    /// <summary>
    /// Annule entièrement le momumtum et fait dasher horizontalement l'objet dans la direction indiquée
    /// </summary>
    public void Dasher()
    {
        if (dashCorout == null)
        {
            hasJumped = false;
            hasDashed = true;
            animator.SetTrigger("Dash");
            SoundManager.PlaySoundEffectIns(Enums.SoundEffect.DASH);
            dashCorout = StartCoroutine(DashCoroutine());
        }
    }

    public void LootCapa(Enums.Skills skill)
    {
        capacities[(int)(skill)] = true;
    }

    protected virtual void LaunchCapa(Enums.Skills skill)
    {
    }

    public virtual void Die()
    {
    }

    private IEnumerator DashCoroutine()
    {
        float startingTime = Time.time;
        float oldMaxSpeed = maxSpeed;
        maxSpeed = dashPower;
        yield return null;
        while (Time.time < startingTime + dashDuration)
        {
            rbody.velocity = new Vector2(dashPower * -transform.localScale.x / (Mathf.Abs(transform.localScale.x)), 0);
            yield return null;
        }
        maxSpeed = oldMaxSpeed;
        yield return null;
        dashCorout = null;
    }
}