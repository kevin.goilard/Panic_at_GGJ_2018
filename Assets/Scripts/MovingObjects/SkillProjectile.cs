﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillProjectile : MovingObject
{
    [SerializeField]
    private TextMesh text;

    [SerializeField]
    private string jump;

    [SerializeField]
    private string sword;

    [SerializeField]
    private string dash;

    [HideInInspector]
    public Enums.Skills skill;

    public void Init(Enums.Skills s, int direction)
    {
        rbody = GetComponent<Rigidbody2D>();
        skill = s;
        switch (s)
        {
            case Enums.Skills.DOUBLE_JUMP:
                text.text = jump;
                break;

            case Enums.Skills.SWORD_HIT:
                text.text = sword;
                break;

            case Enums.Skills.DASH:
                text.text = dash;
                break;

            default:
                break;
        }
        rbody.velocity = new Vector2(direction * maxSpeed, 1);
    }

    public void InitRandom(Enums.Skills s, int direction)
    {
        rbody = GetComponent<Rigidbody2D>();
        skill = s;
        switch (s)
        {
            case Enums.Skills.DOUBLE_JUMP:
                text.text = jump;
                break;

            case Enums.Skills.SWORD_HIT:
                text.text = sword;
                break;

            case Enums.Skills.DASH:
                text.text = dash;
                break;

            default:
                break;
        }
        rbody.velocity = new Vector2(Random.Range(-3, 3), maxSpeed);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Player p = collision.gameObject.GetComponent<Player>();
        if (p != null)
        {
            p.LootCapa(skill);
            Destroy(this.gameObject);
            return;
        }
        Enemy e = collision.gameObject.GetComponent<Enemy>();
        if (e != null)
        {
            e.LootCapa(skill);
            Destroy(this.gameObject);
            return;
        }
    }

    public override void Die()
    {
        Player.Instance.LootCapa(skill);
        Destroy(this.gameObject);
    }
}