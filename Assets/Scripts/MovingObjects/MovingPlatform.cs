﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MovingObject
{
    private Vector3 target;

    [SerializeField]
    private List<Vector3> targetlist = new List<Vector3>();

    private int index = 0;
    private bool isReturning = false;

    protected override void OverridableStart()
    {
        base.OverridableStart();
        target = targetlist[0];
    }

    private void LateUpdate()
    {
        if (Vector2.Distance(transform.position, target) < 0.2f || target == null)
            ChangeTarget();
        rbody.velocity = (target - transform.position).normalized * maxSpeed;
    }

    public override void Die()
    {
    }

    private void ChangeTarget()
    {
        if (!isReturning && index + 1 < targetlist.Count)
        {
            index++;
            target = targetlist[index];
        }
        else if (!isReturning)
        {
            index--;
            target = targetlist[index];
            isReturning = true;
        }
        else if (isReturning && index - 1 >= 0)
        {
            index--;
            target = targetlist[index];
        }
        else if (isReturning)
        {
            index++;
            target = targetlist[index];
            isReturning = false;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        for (int i = 0; i < targetlist.Count; i++)
        {
            Gizmos.DrawSphere(targetlist[i], 0.2f);
        }
        for (int i = 1; i < targetlist.Count; i++)
        {
            Gizmos.DrawLine(targetlist[i - 1], targetlist[i]);
        }
    }
}