﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTool : MonoBehaviour
{
    public void RegenerateCollider()
    {
        BoxCollider2D[] boxColliders = FindObjectsOfType<BoxCollider2D>();
        foreach (BoxCollider2D b in boxColliders)
        {
            SpriteRenderer s = b.GetComponent<SpriteRenderer>();
            if (s != null)
                b.size = s.size;
        }
        PolygonCollider2D[] polygonColliders = FindObjectsOfType<PolygonCollider2D>();
        foreach (PolygonCollider2D p in polygonColliders)
        {
            GameObject g = p.gameObject;
            DestroyImmediate(p);
            g.AddComponent<PolygonCollider2D>();
        }
    }

    public void GenerateLimits()
    {
        StaticPlatform[] staticPlatforms = FindObjectsOfType<StaticPlatform>();
        foreach (StaticPlatform b in staticPlatforms)
        {
            foreach (Transform t in b.GetComponentsInChildren<Transform>())
            {
                if (t != b.transform)
                    DestroyImmediate(t.gameObject);
            }
            SpriteRenderer s = b.GetComponent<SpriteRenderer>();
            if (s != null)
            {
                GameObject g1 = new GameObject();
                g1.transform.SetParent(b.gameObject.transform);
                g1.transform.position = new Vector3(b.transform.position.x - s.size.x / 2, b.transform.position.y + 1, 0);
                BoxCollider2D coll = g1.AddComponent<BoxCollider2D>();
                coll.size = new Vector2(0.5f, 2);
                coll.isTrigger = true;

                GameObject g2 = new GameObject();
                g2.transform.SetParent(b.gameObject.transform);
                g2.transform.position = new Vector3(b.transform.position.x + s.size.x / 2, b.transform.position.y + 1, 0);
                BoxCollider2D coll2 = g2.AddComponent<BoxCollider2D>();
                coll2.size = new Vector2(0.5f, 2);
                coll2.isTrigger = true;
            }
        }

        MovingPlatform[] movingPlatform = FindObjectsOfType<MovingPlatform>();
        foreach (MovingPlatform m in movingPlatform)
        {
            foreach (Transform t in m.GetComponentsInChildren<Transform>())
            {
                if (t != m.transform)
                    DestroyImmediate(t.gameObject);
            }
            SpriteRenderer s = m.GetComponent<SpriteRenderer>();
            if (s != null)
            {
                GameObject g1 = new GameObject();
                g1.transform.SetParent(m.gameObject.transform);
                g1.transform.position = new Vector3(m.transform.position.x - s.size.x / 2, m.transform.position.y + 1, 0);
                BoxCollider2D coll = g1.AddComponent<BoxCollider2D>();
                coll.size = new Vector2(0.5f, 2);
                coll.isTrigger = true;

                GameObject g2 = new GameObject();
                g2.transform.SetParent(m.gameObject.transform);
                g2.transform.position = new Vector3(m.transform.position.x + s.size.x / 2, m.transform.position.y + 1, 0);
                BoxCollider2D coll2 = g2.AddComponent<BoxCollider2D>();
                coll2.size = new Vector2(0.5f, 2);
                coll2.isTrigger = true;
            }
        }

        UniqueMovingPlatform[] uniqueMovingPlatform = FindObjectsOfType<UniqueMovingPlatform>();
        foreach (UniqueMovingPlatform m in uniqueMovingPlatform)
        {
            foreach (Transform t in m.GetComponentsInChildren<Transform>())
            {
                if (t != m.transform)
                    DestroyImmediate(t.gameObject);
            }
            SpriteRenderer s = m.GetComponent<SpriteRenderer>();
            if (s != null)
            {
                GameObject g1 = new GameObject();
                g1.transform.SetParent(m.gameObject.transform);
                g1.transform.position = new Vector3(m.transform.position.x - s.size.x / 2, m.transform.position.y + 1, 0);
                BoxCollider2D coll = g1.AddComponent<BoxCollider2D>();
                coll.size = new Vector2(0.5f, 2);
                coll.isTrigger = true;

                GameObject g2 = new GameObject();
                g2.transform.SetParent(m.gameObject.transform);
                g2.transform.position = new Vector3(m.transform.position.x + s.size.x / 2, m.transform.position.y + 1, 0);
                BoxCollider2D coll2 = g2.AddComponent<BoxCollider2D>();
                coll2.size = new Vector2(0.5f, 2);
                coll2.isTrigger = true;
            }
        }

        EnemyUniqueMovingPlatform[] enemyUniqueMovingPlatform = FindObjectsOfType<EnemyUniqueMovingPlatform>();
        foreach (EnemyUniqueMovingPlatform m in enemyUniqueMovingPlatform)
        {
            foreach (Transform t in m.GetComponentsInChildren<Transform>())
            {
                if (t != m.transform)
                    DestroyImmediate(t.gameObject);
            }
            SpriteRenderer s = m.GetComponent<SpriteRenderer>();
            if (s != null)
            {
                GameObject g1 = new GameObject();
                g1.transform.SetParent(m.gameObject.transform);
                g1.transform.position = new Vector3(m.transform.position.x - s.size.x / 2, m.transform.position.y + 1, 0);
                BoxCollider2D coll = g1.AddComponent<BoxCollider2D>();
                coll.size = new Vector2(0.5f, 2);
                coll.isTrigger = true;

                GameObject g2 = new GameObject();
                g2.transform.SetParent(m.gameObject.transform);
                g2.transform.position = new Vector3(m.transform.position.x + s.size.x / 2, m.transform.position.y + 1, 0);
                BoxCollider2D coll2 = g2.AddComponent<BoxCollider2D>();
                coll2.size = new Vector2(0.5f, 2);
                coll2.isTrigger = true;
            }
        }
    }
}