﻿using UnityEngine;

/// <summary>
/// Classe comme <see cref="Singleton{T}"/>. Mais utilise un prefab situé dans Resources/PrefabSingleton/T.
/// </summary>
/// <typeparam name="T"></typeparam>
public class PrefabSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static string prefabPath = "PrefabSingleton/" + typeof(T);

    private static T _instance;

    private static object _lock = new object();

    public static T Instance
    {
        get
        {
            lock (_lock)
            {
                if (_instance == null)
                {
                    _instance = (T)FindObjectOfType(typeof(T));

                    if (FindObjectsOfType(typeof(T)).Length > 1)
                    {
                        Debug.LogError("[PrefabSingletonSingleton] Something went really wrong " +
                            " - there should never be more than 1 singleton!" +
                            " Reopening the scene might fix it.");

                        return _instance;
                    }

                    if (_instance == null)
                    {
                        _instance = Instantiate(Resources.Load<GameObject>(prefabPath)).GetComponent<T>();

                        Debug.Log("[PrefabSingletonSingleton] An instance of " + typeof(T) +
                            " is needed in the scene, so the prefab was instantiate.");
                        if (_instance == null)
                        {
                            Debug.Log("No script of type " + typeof(T) + " on the prefab in the instantiated prafab.");
                        }
                    }
                    else
                    {
                        Debug.Log("[PrefabSingletonSingleton] Using instance already created: " +
                            _instance.gameObject.name);
                    }
                }

                return _instance;
            }
        }
    }

    /* private static bool applicationIsQuitting = false;
     /// <summary>
     /// When Unity quits, it destroys objects in a random order.
     /// In principle, a Singleton is only destroyed when application quits.
     /// If any script calls Instance after it have been destroyed,
     ///   it will create a buggy ghost object that will stay on the Editor scene
     ///   even after stopping playing the Application. Really bad!
     /// So, this was made to be sure we're not creating that buggy ghost object.
     /// </summary>
     public virtual void OnDestroy()
     {
         applicationIsQuitting = true;
     }*/
}