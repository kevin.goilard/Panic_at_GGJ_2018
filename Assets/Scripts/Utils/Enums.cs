﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Enums
{
    public enum ColorType
    {
        ALLY, ENEMY, NEUTRAL
    }

    public enum EventTypeEnum
    { ALLY_COLOR_CHANGED, ENNEMI_COLOR_CHANGED, NEUTRAL_COLOR_CHANGED }

    public enum Directions
    { LEFT, RIGHT }

    public enum Skills
    { DOUBLE_JUMP, SWORD_HIT, DASH }

    public enum Music
    { NONE, MENU, TUTO, NORTH, SOUTH, EST, WEST, CREDITS, GLITCH }

    public enum SoundEffect
    { NONE, ATTACK, DASH, DASH_PLAYER, ENEMY_HIT, PLAYER_HIT, FIN_NIVEAU, GAMEOVER, GIMMICK, NEWGAME, PORTAIL, RECEPTION_SAUT, TRANSITION_OUTER_WORLD, PIECE }

    public enum Objectives
    {
        COIN, DASH, JUMP, SWORD
    }
}