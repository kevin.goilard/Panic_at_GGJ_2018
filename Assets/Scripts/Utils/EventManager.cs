﻿using System;
using System.Collections.Generic;

public delegate void Callback();

public delegate void Callback<T>(T arg);

public static class EventManager
{
    /// <summary>
    /// Dictionnary that translate the events to callbacks, methods to call
    /// when the event is raised
    /// </summary>
    private static Dictionary<Enums.EventTypeEnum, List<Delegate>> dicoEventAction = new Dictionary<Enums.EventTypeEnum, List<Delegate>>();

    public static void ClearEventDictionnary(Enums.EventTypeEnum even)
    {
        if (dicoEventAction.ContainsKey(even))
        {
            dicoEventAction[even].Clear();
        }
    }

    public static void ClearAllEventsDictionnary()
    {
        dicoEventAction.Clear();
    }

    /// <summary>
    /// Subscribe a method to an event
    /// </summary>
    /// <param name="even">The event</param>
    /// <param name="method">The method</param>
    public static void AddActionToEvent(Enums.EventTypeEnum even, Callback method)
    {
        if (!dicoEventAction.ContainsKey(even))
        {
            dicoEventAction.Add(even, new List<Delegate>());
        }
        dicoEventAction[even].Add(method);
    }

    /// <summary>
    /// Subscribe a method to an event
    /// </summary>
    /// <param name="even">The event</param>
    /// <param name="method">The method</param>
    public static void AddActionToEvent<T>(Enums.EventTypeEnum even, Callback<T> method)
    {
        if (!dicoEventAction.ContainsKey(even))
        {
            dicoEventAction.Add(even, new List<Delegate>());
        }
        dicoEventAction[even].Add(method);
    }

    /// <summary>c
    /// Unsubscribe a method to an event
    /// </summary>
    /// <param name="even">The event to unsubscribe</param>
    /// <param name="method">The method to unsubscribe</param>
    public static void RemoveActionFromEvent(Enums.EventTypeEnum even, Callback method)
    {
        if (dicoEventAction.ContainsKey(even))
        {
            dicoEventAction[even].Remove(method);
        }
    }

    /// <summary>
    /// Unsubscribe a method to an event
    /// </summary>
    /// <param name="even">The event to unsubscribe</param>
    /// <param name="method">The method to unsubscribe</param>
    public static void RemoveActionFromEvent<T>(Enums.EventTypeEnum even, Callback<T> method)
    {
        if (dicoEventAction.ContainsKey(even))
        {
            dicoEventAction[even].Remove(method);
        }
    }

    /// <summary>
    /// Raise an event, so it will trigger all the subscribed methods
    /// </summary>
    /// <param name="eventToCall">The event to raise</param>
    public static void Raise(Enums.EventTypeEnum eventToCall)
    {
        if (!dicoEventAction.ContainsKey(eventToCall))
        {
            return;
        }
        if (dicoEventAction[eventToCall].Count < 1)
        {
            return;
        }
        foreach (Delegate d in dicoEventAction[eventToCall].ToArray())
        {
            Callback c = (Callback)d;
            if (c != null)
                c();
        }
    }

    /// <summary>
    /// Raise an event, so it will trigger all the subscribed methods
    /// </summary>
    /// <param name="eventToCall">The event to raise</param>
    /// <param name="arg">The argument that we pass with the event</param>

    public static void Raise<T>(Enums.EventTypeEnum eventToCall, T arg)
    {
        if (!dicoEventAction.ContainsKey(eventToCall) || dicoEventAction[eventToCall].Count < 1)
        {
            return;
        }
        foreach (Delegate d in dicoEventAction[eventToCall].ToArray())
        {
            Callback<T> c = (Callback<T>)d;
            if (c != null)
                c(arg);
        }
    }
}