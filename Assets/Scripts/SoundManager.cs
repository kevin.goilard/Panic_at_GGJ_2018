﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [Header("Parameters")]
    [SerializeField]
    private AudioSource musique;

    [SerializeField]
    private AudioSource soundEffect;

    [Header("Music")]
    [SerializeField]
    private AudioClip menu;

    [SerializeField]
    private AudioClip tuto;

    [SerializeField]
    private AudioClip glitch;

    [SerializeField]
    private AudioClip north;

    [SerializeField]
    private AudioClip south;

    [SerializeField]
    private AudioClip est;

    [SerializeField]
    private AudioClip west;

    [SerializeField]
    private AudioClip credits;

    [Header("SoundEffect")]
    [SerializeField]
    private AudioClip attack;

    [SerializeField]
    private AudioClip dash;

    [SerializeField]
    private AudioClip dashPlayer;

    [SerializeField]
    private AudioClip enemyHit;

    [SerializeField]
    private AudioClip playerHit;

    [SerializeField]
    private AudioClip finNiveau;

    [SerializeField]
    private AudioClip gameOver;

    [SerializeField]
    private AudioClip gimmick;

    [SerializeField]
    private AudioClip newGame;

    [SerializeField]
    private AudioClip portail;

    [SerializeField]
    private AudioClip receptionSaut;

    [SerializeField]
    private AudioClip transitionOuterWorld;

    [SerializeField]
    private AudioClip piece;

    public static SoundManager instance;

    private Coroutine fade;

    private float volume;

    private Enums.Music actualMusic = Enums.Music.NONE;

    // Use this for initialization
    private void Start()
    {
        if (instance != null)
            Destroy(this);
        instance = this;
        DontDestroyOnLoad(this);
        musique.priority = 250;
        musique.loop = true;

        volume = musique.volume;

        soundEffect.priority = 50;
        soundEffect.loop = true;
    }

    private void PlayMusic(Enums.Music music)
    {
        if (actualMusic == music)
            return;
        musique.Stop();
        musique.volume = volume;
        AudioClip nextMus = null;
        switch (music)
        {
            case Enums.Music.MENU:
                nextMus = menu;
                break;

            case Enums.Music.TUTO:
                nextMus = tuto;
                break;

            case Enums.Music.NORTH:
                nextMus = north;
                break;

            case Enums.Music.SOUTH:
                nextMus = south;
                break;

            case Enums.Music.EST:
                nextMus = est;
                break;

            case Enums.Music.WEST:
                nextMus = west;
                break;

            case Enums.Music.CREDITS:
                nextMus = credits;
                break;

            case Enums.Music.GLITCH:
                nextMus = glitch;
                break;

            case Enums.Music.NONE:
                break;
        }
        if (nextMus == null)
            return;
        musique.clip = nextMus;
        musique.Play();
        actualMusic = music;
    }

    private void PlaySoundEffect(Enums.SoundEffect sound)
    {
        switch (sound)
        {
            case Enums.SoundEffect.ATTACK:
                soundEffect.PlayOneShot(attack);
                break;

            case Enums.SoundEffect.DASH:
                soundEffect.PlayOneShot(dash);
                break;

            case Enums.SoundEffect.DASH_PLAYER:
                soundEffect.PlayOneShot(dashPlayer);
                break;

            case Enums.SoundEffect.ENEMY_HIT:
                soundEffect.PlayOneShot(enemyHit);
                break;

            case Enums.SoundEffect.PLAYER_HIT:
                soundEffect.PlayOneShot(playerHit);
                break;

            case Enums.SoundEffect.FIN_NIVEAU:
                soundEffect.PlayOneShot(finNiveau);
                break;

            case Enums.SoundEffect.GAMEOVER:
                soundEffect.PlayOneShot(gameOver);
                break;

            case Enums.SoundEffect.GIMMICK:
                soundEffect.PlayOneShot(gimmick);
                break;

            case Enums.SoundEffect.NEWGAME:
                soundEffect.PlayOneShot(newGame);
                break;

            case Enums.SoundEffect.PORTAIL:
                soundEffect.PlayOneShot(portail);
                break;

            case Enums.SoundEffect.RECEPTION_SAUT:
                soundEffect.PlayOneShot(receptionSaut);
                break;

            case Enums.SoundEffect.TRANSITION_OUTER_WORLD:
                soundEffect.PlayOneShot(transitionOuterWorld);
                break;

            case Enums.SoundEffect.PIECE:
                soundEffect.PlayOneShot(piece);
                break;
        }
    }

    private IEnumerator FadeInto(Enums.Music music)
    {
        float end = Time.time + 1.5f;
        while (Time.time < end)
        {
            musique.volume = volume * (end - Time.time) / 1.5f;
            yield return null;
        }
        PlayMusic(music);
        fade = null;
    }

    /// <summary>
    /// Joue la musique en parametre sans transition
    /// </summary>
    /// <param name="music">Musique à jouer</param>
    public static void PlayMusicIns(Enums.Music music)
    {
        if (instance != null)
            instance.PlayMusic(music);
    }

    /// <summary>
    /// Joue le son spécifié
    /// </summary>
    /// <param name="sound">Son à jouer</param>
    public static void PlaySoundEffectIns(Enums.SoundEffect sound)
    {
        if (instance != null)
            instance.PlaySoundEffect(sound);
    }

    /// <summary>
    /// Joue la musique en parametre après un fade out de 1.5 seconde
    /// </summary>
    /// <param name="music">Musique à jouer</param>
    public static void FadeIntoIns(Enums.Music music)
    {
        if (instance != null)
        {
            if (instance.actualMusic == music)
                return;
            if (instance.fade != null)
                instance.StopCoroutine(instance.fade);
            instance.fade = instance.StartCoroutine(instance.FadeInto(music));
        }
    }
}