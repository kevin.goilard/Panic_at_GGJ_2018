﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillThings : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        MovingObject o = collision.gameObject.GetComponent<MovingObject>();
        if (o != null)
        {
            o.Die();
            if (o.GetComponent<Enemy>() != null && GetComponentInParent<Enemy>() != null && GetComponentInParent<SkillProjectile>() != null)
            {
                Player.enemyKill++;
            }
        }
    }
}