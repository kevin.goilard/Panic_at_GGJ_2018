﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour {

    public GameObject credit;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartButtonClick(int sceneValue)
    {
        SceneManager.LoadSceneAsync(sceneValue);
    }

    /// <summary>
    /// Permet de quitter le jeu
    /// </summary>
    void QuitButtonClick()
    {
        Application.Quit(); 
    }

}
