﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Interactable : ColoredObject
{
    [SerializeField]
    private UnityEvent eventsToCall;

    [SerializeField]
    private bool destroyAtCall = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Player>() != null)
        {
            eventsToCall.Invoke();
            if (destroyAtCall)
                Destroy(gameObject);
        }
    }

    public void ForceDash()
    {
        Player.Instance.Dasher();
    }

    public void ChangeScene(int id)
    {
        Player.Instance.MakeTransition(id);
    }

    public void ChangeSceneBis(int id)
    {
        Player.Instance.MakeTransitionBis(id);
    }
}