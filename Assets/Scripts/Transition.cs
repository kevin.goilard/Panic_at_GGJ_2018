﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Transition : MonoBehaviour
{
    [SerializeField]
    protected SpriteRenderer ecranNoirFadeOut;

    [SerializeField]
    protected SpriteRenderer portail;

    public void ChangeScene(int id)
    {
        StartCoroutine(AnimFinNiveau(id));
    }

    public void ChangeSceneBis(int id)
    {
        StartCoroutine(AnimFinNiveauBis(id));
    }

    private IEnumerator AnimFinNiveau(int id)
    {
        float end = Time.time + 3.5f;
        Vector3 scale = new Vector3(8, 8, 1);
        SoundManager.PlaySoundEffectIns(Enums.SoundEffect.FIN_NIVEAU);
        while (Time.time < end)
        {
            transform.localScale = scale * (end - Time.time) / 3.5f;
            yield return null;
        }
        transform.localScale = Vector3.zero;

        end = Time.time + 0.5f;
        while (Time.time < end)
            yield return null;
        SceneManager.LoadSceneAsync(id);
    }

    private IEnumerator AnimFinNiveauBis(int id)
    {
        float end = Time.time + 3.5f;
        Vector3 scale = new Vector3(8, 8, 1);
        SoundManager.PlaySoundEffectIns(Enums.SoundEffect.FIN_NIVEAU);
        while (Time.time < end)
        {
            transform.localScale = scale * (end - Time.time) / 3.5f;
            yield return null;
        }
        transform.localScale = Vector3.zero;

        end = Time.time + 1f;
        while (Time.time < end)
            yield return null;

        portail.gameObject.SetActive(true);

        end = Time.time + 1f;
        while (Time.time < end)
        {
            portail.color = new Color(0, 0, 0, 1 - (end - Time.time) / 1f);
            yield return null;
        }

        end = Time.time + 1f;
        while (Time.time < end)
        {
            ecranNoirFadeOut.color = new Color(0, 0, 0, 1 - (end - Time.time) / 1f);
            yield return null;
        }
        ecranNoirFadeOut.color = Color.black;
        end = Time.time + 0.5f;
        while (Time.time < end)
            yield return null;

        SceneManager.LoadSceneAsync(id);
    }
}