﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchMusic : MonoBehaviour
{
    [SerializeField]
    private GameObject soundManager;

    [SerializeField]
    private Enums.Music music;

    // Use this for initialization
    private void Start()
    {
        StartCoroutine(Launch());
    }

    private IEnumerator Launch()
    {
        yield return new WaitForSeconds(0.05f);
        if (SoundManager.instance == null)
            Instantiate(soundManager);
        yield return new WaitForSeconds(0.05f);
        SoundManager.PlayMusicIns(music);
    }
}