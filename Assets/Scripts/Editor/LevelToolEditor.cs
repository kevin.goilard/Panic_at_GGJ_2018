﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelTool))]
public class LevelToolEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        LevelTool tool = (LevelTool)target;
        if (GUILayout.Button("Recalculate colliders"))
        {
            tool.RegenerateCollider();
        }

        if (GUILayout.Button("Generate limits"))
        {
            tool.GenerateLimits();
        }
    }
}