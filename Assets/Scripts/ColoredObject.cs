﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColoredObject : MonoBehaviour
{
    [SerializeField]
    private Enums.ColorType color = Enums.ColorType.NEUTRAL;

    [SerializeField]
    private bool changeColor = true;

    private void Start()
    {
        if (changeColor)
        {
            switch (color)
            {
                case Enums.ColorType.ALLY:
                    EventManager.AddActionToEvent<Color>(Enums.EventTypeEnum.ALLY_COLOR_CHANGED, ChangeColor);
                    break;

                case Enums.ColorType.ENEMY:
                    EventManager.AddActionToEvent<Color>(Enums.EventTypeEnum.ENNEMI_COLOR_CHANGED, ChangeColor);
                    break;

                case Enums.ColorType.NEUTRAL:
                    EventManager.AddActionToEvent<Color>(Enums.EventTypeEnum.NEUTRAL_COLOR_CHANGED, ChangeColor);
                    break;

                default:
                    break;
            }
        }
        OverridableStart();
    }

    protected virtual void OverridableStart()
    {
    }

    protected void ChangeColor(Color newColor)
    {
        foreach (SpriteRenderer s in GetComponentsInChildren<SpriteRenderer>())
        {
            s.material.color = newColor;
        }
        foreach (Text s in GetComponentsInChildren<Text>())
        {
            s.color = newColor;
        }
    }

    private void OnDestroy()
    {
        if (changeColor)
        {
            switch (color)
            {
                case Enums.ColorType.ALLY:
                    EventManager.RemoveActionFromEvent<Color>(Enums.EventTypeEnum.ALLY_COLOR_CHANGED, ChangeColor);
                    break;

                case Enums.ColorType.ENEMY:
                    EventManager.RemoveActionFromEvent<Color>(Enums.EventTypeEnum.ENNEMI_COLOR_CHANGED, ChangeColor);
                    break;

                case Enums.ColorType.NEUTRAL:
                    EventManager.RemoveActionFromEvent<Color>(Enums.EventTypeEnum.NEUTRAL_COLOR_CHANGED, ChangeColor);
                    break;

                default:
                    break;
            }
        }
    }
}