﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicDoor : Ground
{
    [SerializeField]
    private Enums.Objectives objectif;

    private void OnEnable()
    {
        bool explode = false;
        switch (objectif)
        {
            case Enums.Objectives.COIN:
                explode = Player.coins >= 50;
                break;

            case Enums.Objectives.DASH:
                explode = Player.enemyDash >= 50;
                break;

            case Enums.Objectives.JUMP:
                explode = Player.enemyJump >= 50;
                break;

            case Enums.Objectives.SWORD:
                explode = Player.enemyKill >= 50;
                break;

            default:
                break;
        }
        if (explode)
            Destroy(gameObject);
    }
}