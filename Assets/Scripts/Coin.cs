﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : ColoredObject
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        SoundManager.PlaySoundEffectIns(Enums.SoundEffect.PIECE);
        if (collision.GetComponent<Player>() != null)
            Destroy(gameObject);
    }
}